package oak.forge.commons.context;

import oak.forge.commons.aggregates.api.aggregate.AggregateLoader;
import oak.forge.commons.aggregates.api.command.exception.CommandRegisteringException;
import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.aggregates.impl.aggregate.AbstractAggregateRoot;
import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class ContextTest {

    private static final String COMMAND_NAME = "comnand";

    @Mock
    MessageBus messageBus = mock(MessageBus.class);
    EventStore eventStore = mock(EventStore.class);
    AggregateLoader aggregateLoader = mock(AggregateLoader.class);

    private final Context context = new Context();

    private static class TestCommand extends Command {
        private static final long serialVersionUID = -3370861905412125458L;
        public String c;
    }

    class State extends AggregateState {
        private static final long serialVersionUID = 1L;

        State(UUID aggregateId) {
            super(aggregateId);
        }
    }

    private class TestAggregate extends AbstractAggregateRoot<State> {

        public TestAggregate() {
            super(messageBus, eventStore, aggregateLoader, context);
        }

        @Override
        public State initState(UUID aggregateId) {
            return new State(aggregateId);
        }
    }

    private final TestAggregate testAggregate = new TestAggregate();

    @Test
    public void should_register_command_mapping() {
        // when
        Context ctx = context.registerCommandMapping(COMMAND_NAME, TestCommand.class, testAggregate);

        // then
        assertThat(ctx).isSameAs(context);
        assertThat(context.getCommandClass(COMMAND_NAME)).isSameAs(TestCommand.class);
    }

    @Test
    public void should_not_register_same_command_mapping_twice() {
        // given
        context.registerCommandMapping(COMMAND_NAME, TestCommand.class, testAggregate);

        // then
        assertThatExceptionOfType(CommandRegisteringException.class)
                .isThrownBy(() -> context.registerCommandMapping(COMMAND_NAME, TestCommand.class, testAggregate));
    }

}
